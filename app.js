let modules = require('./modules').create();

// объявление модуля A
modules.define('A', [], (provide) => {
  provide(1)
});
// объявление модуля B
modules.define('B', [], (provide) => {
  setTimeout(() => {
    provide(2)
  }, 1000);
});

// Полезная логика
modules.require(['A', 'B'], (A, B) => {
  console.log(A + B); // 1 + 2 = 3
});

// вывод для проверки асинхронности require
console.log(4); // 4
